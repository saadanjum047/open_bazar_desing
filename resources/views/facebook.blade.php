<html>
<head>
  <title>Your Website Title</title>
  <!-- Comment #1: OG Tags -->
  <meta property="og:url"           content="http://site.openbazar.nl" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Your Website Title" />
  <meta property="og:description"   content="Your description" />
<meta property="og:image"         content="https://cdn.pixabay.com/photo/2021/02/05/21/10/church-5985941_960_720.jpg" />
</head>
<body>

  <!-- Comment #2: SDK -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Comment #3: Content -->
  <h1>Your Headline</h1>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget tristique nibh,
    vel consequat purus. Praesent molestie, turpis ut ultrices commodo, felis arcu cursus enim,
    vel porttitor ante quam vel lacus. Quisque at laoreet sapien. Proin nec purus dolor. Integer
    pharetra nec ligula a condimentum. Fusce sem orci, porta ut felis id, commodo imperdiet risus.
    Download App Link :http://site.openbazar.nl
  </p>

  <!-- Comment #4: Plugin Code -->
  <div class="fb-quote"></div>

</body>
</html>


<html>
<head>
<title>Your Website Title</title>
<!-- You can use Open Graph tags to customize link previews.
Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
<meta property="og:url"           content="http://site.openbazar.nl/" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="Your Website Title" />
<meta property="og:description"   content="Your description" />
<!-- <meta property="og:qoute"   content="Your Qoute" /> -->
<meta property="og:image"         content="https://cdn.pixabay.com/photo/2021/02/05/21/10/church-5985941_960_720.jpg" />
</head>
<body>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your share button code -->
<div class="fb-share-button"
data-href="http://site.openbazar.nl"
data-layout="button">
</div>


<h1>Sharing using FB.ui() Dialogs</h1>

<p>Below are some simple examples of how to use UI dialogs in a web page.</p>

<h3>Sharing Links</h3>

<div id="shareBtn" class="btn btn-success clearfix">Share Dialog</div>

<p>The Share Dialog enables you to share links to a person's profile without them having to use Facebook Login. <a href="https://developers.facebook.com/docs/sharing/reference/share-dialog">Read our Share Dialog guide</a> to learn more about how it works.</p>

<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>
  window.FB.init({
  appID: "568498240429695",
  frictionlessRequests: true,
  init: true,
  level: "debug",
  signedRequest: null,
  status: true,
  version: "v3.2",
  viewMode: "website",
  autoRun: true
});
</script>

<script>

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '568498240429695',
      xfbml      : true,
      version    : 'v2.1'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));


document.getElementById('shareBtn').onclick = function() {
  FB.ui({
    display: 'popup',

    method: 'share',
    href: 'https://developers.facebook.com/docs/',
  }, function(response){});
}
</script>

<h3>Publishing Open Graph Stories</h3>

<p>The Share Dialog can also be used to publish Open Graph stories without using Facebook Login or the Graph API. <a href="https://developers.facebook.com/docs/sharing/reference/share-dialog">Read our Share Dialog guide</a> to learn more about how it works.</p>

<div id="ogBtn" class="btn btn-success clearfix">Simple OG Dialog</div>

<script>
document.getElementById('ogBtn').onclick = function() {
  FB.ui({
    display: 'popup',
    method: 'share_open_graph',
    action_type: 'og.likes',
    action_properties: JSON.stringify({
        object:'https://developers.facebook.com/docs/',
    })
  }, function(response){});
}
</script>

<h3>Sending App Requests</h3>

<p><a href="https://developers.facebook.com/docs/games/requests/">Requests</a> can be sent by any Facebook Apps that are categorised as Games and have a Canvas, iOS, or Android implementation. The JavaScript SDK enables web Canvas games to send requests. <a href="https://developers.facebook.com/docs/games/requests/">Read our guide to Requests</a> to learn more and see more complex examples that you could use.</p>

<div id="requestsBtn" class="btn btn-success clearfix">Basic Request Dialog</div>

<script>
document.getElementById('requestsBtn').onclick = function() {
  FB.ui({method: 'apprequests',
      message: 'This is a test message for the requests dialog.'
  }, function(data) {
    Log.info('App Requests Response', data);
  });
}



</script>

</body>
</html>

