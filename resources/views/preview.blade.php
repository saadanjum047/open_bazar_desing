<html>
<head>
  <title>{{$ad['ad_title']}}</title>
  <!-- Comment #1: OG Tags -->

    @php
        $url = 'https://openbazar.nl';
        if(array_key_exists( 'images' , $ad) && $ad['images'] && count(json_decode($ad['images'])) > 0){
            $image = json_decode($ad['images'])[0]->image;
            $featured = $url . '/app-admin/public/images/' . $image;
        }
    @endphp

    <meta property="og:url"           content="{{request()->url()}}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{$ad['ad_title']}}" />
    <meta property="og:description" content="{{$ad['ad_description']}}" >

    @if(isset($featured))
        <meta property="og:image"         content="{{$featured}}" />
    @else
        <meta property="og:image"         content="/bazar/no-image-preview.png" />
    @endif
</head>
<body>
    {{-- {{request()->url()}} --}}
    {{-- {{json_encode($ad)}} --}}
<script>
    window.onload = function() {
        setTimeout( () => {
            console.log('inside')
            window.location.href = '/product/{{$ad["id"]}}/view';
        } , 100);
    }
</script>

</body>
</html>

