window.Vue = require('vue');
import moment from 'moment'

const virtualmin_server = "https://openbazar.nl/app-admin/";
// const virtualmin_server = "http://164.90.197.43/app-admin/";
// const virtualmin_server = "http://main.openbazar.nl/app-admin/";
const server = "https://openbazar.nl/app-admin/";


Vue.filter('formatedDate', function(date) {
    return moment(date).format("D MMM YYYY")
})


Vue.filter('formatDate', function(date) {
    return moment(date).format("D/MM/YYYY")
})

Vue.filter('formatedDateWithDay', function(date) {
    let new_date = new Date(date)
        //   console.log(`date`, date)
        //   console.log(`moment(date)`, moment(date , "DD-MM-YYYY"))
        //   console.log(`new_date`, new_date)
        //   console.log(`new_date(date)`, moment(new_date))
    return moment(date).format("Do MMM YYYY")
        // moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
})

Vue.filter('formatDifferenceDate', function(date) {
    return moment(date).fromNow();
})
Vue.filter('formatDifferenceDateTimestamp', function(timestamp) {
    var d = new Date(parseInt(timestamp));
    return moment(d).fromNow();
})

Vue.filter('timestampToDifference', function(timestamp) {

    var d = new Date(parseInt(timestamp));
    return moment(d).fromNow();
    // return moment(timestamp).format("DD MMM YYYY hh:mm a").fromNow();
    // return moment(timestamp).format("DD MMM YYYY hh:mm a");
})

Vue.filter('timestampToTime', function(timestamp) {

    var d = new Date(parseInt(timestamp));
    // console.log( d , moment(d) , timestamp )
    return moment(d).format("hh:mm A");
    // return moment(timestamp).format("hh:mm A");

    // return moment(timestamp).format("DD MMM YYYY hh:mm a").fromNow();
    // return moment(timestamp).format("DD MMM YYYY hh:mm a");
})

Vue.filter('responseTimeDifference', function(timestamp) {
    let d = new Date(parseInt(timestamp));
    let duration = moment.duration(d);
    let time = 0;
    // 1440
    //   console.log(duration)
    //   console.log(duration.seconds())
    //   console.log(duration.minutes())
    //   console.log(duration.asHours())
    //   console.log(duration.asDays())

    if (duration.asSeconds() < 60) {
        time = ' less then 1 hour'
            // time = duration.asSeconds().toFixed(1) + ' seconds'
            // console.log(time)
    } else if (duration.asSeconds() > 60 && duration.asMinutes() < 60) {
        // time = duration.asMinutes().toFixed(1) + ' minutes'
        time = ' less then 1 hour'
            // console.log(time)

    } else if (duration.asMinutes() > 60 && duration.asHours() < 24) {
        time = Math.ceil(duration.asHours()) + ' hours'
            // console.log(time)

    } else if (duration.asHours() > 24) {
        time = Math.ceil(duration.asDays()) + ' days'

        if (Math.ceil(duration.asDays()) > 7) {
            time = '7 days'
        }
        // console.log(time)

    } else {
        time = Math.ceil(duration.asMinutes()) + ' minutes -'
        console.log(time)
    }
    return time
        // console.log(timestamp , moment.duration(d).minutes())
        // return moment.duration(d).minutes();
})
Vue.filter('timestampToDate', function(timestamp) {

    var d = new Date(parseInt(timestamp));
    return moment(d).format("DD MMM YYYY");

    // return moment(timestamp).format("DD MMM YYYY hh:mm a").fromNow();
    // return moment(timestamp).format("DD MMM YYYY hh:mm a");
})

Vue.filter('numberFromString', function(string) {
    var num = string.replace(/^\D+/g, '');
    return num
        // return moment(timestamp).format("DD MMM YYYY hh:mm a");
})


Vue.filter('todayDate', function(date) {
    return moment().format("Do MMM");
})

Vue.filter('currencyConversion', function(euros) {

    // euros = euros.replace(',' , '.' , parseInt(euros) )
    // console.log(store.getters.conversionRate , euros , store.getters.conversionRate * euros )

    return (store.getters.conversionRate * euros).toFixed(2)

})

Vue.filter('serverPath', function(image) {
    let regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    if (regexp.test(image)) {
        return image;
    } else {
        return virtualmin_server + 'public/images/' + image;
    }
})

Vue.filter('rootServerPath', function(image) {
    let regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    if (regexp.test(image)) {
        return image;
    } else {
        return server + 'public/images/' + image;
    }
})
