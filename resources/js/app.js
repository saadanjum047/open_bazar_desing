/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
Vue.use(VueRouter)
Vue.config.devtools = true;


import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'

window.toastr = require('toastr')

Vue.use(VueToastr2)

import router from './route';

// import moment from 'moment';


// Adding all local components
import './components.js';

// adding all the NPM Modules and components
import './imports.js';

import './moment.js';

import Vuex from 'vuex'
Vue.use(Vuex)
    // used for vuex

import 'es6-promise/auto'
import createPersistedState from "vuex-persistedstate";
import persistantDate from './store.js'
import otherData from './other-data.js'

import Cookies from "js-cookie";

const store = new Vuex.Store({
    modules: {
        persistantDate,
        otherData,
    },
    plugins: [createPersistedState({
        paths: ['persistantDate'],
        // storage: window.sessionStorage,
        storage: {
            getItem: (key) => Cookies.get(key),
            setItem: (key, value) =>
                Cookies.set(key, value, { expires: 1, secure: false }),
            removeItem: (key) => Cookies.remove(key)
        }


    })],
})

window.store = store


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


// import Ads from 'vue-google-adsense'

// Vue.use(require('vue-script2'))
// import Adsense from 'vue-google-adsense/dist/Adsense.min.js'
// import InArticleAdsense from 'vue-google-adsense/dist/InArticleAdsense.min.js'
// import InFeedAdsense from 'vue-google-adsense/dist/InFeedAdsense.min.js'

// Vue.use(require('vue-script2'))

// Vue.use(Adsense)
// Vue.use(InArticleAdsense)
// Vue.use(InFeedAdsense)

import Ads from 'vue-google-adsense'
Vue.use(require('vue-script2'))
Vue.use(Ads.Adsense)
Vue.use(Ads.InFeedAdsense)
Vue.use(Ads.InArticleAdsense)
// Vue.use(Ads.AutoAdsense, { adClient: '7434801210', isNewAdsCode: true })



// import ClickOutside from 'vue-click-outside'

// Vue.directive('click-outside', ClickOutside)


const vue = new Vue({
    el: '#vue',
    router,
    store,
    computed: {
        numberOfAdsForAdsense(){
            let number = 12
            if(window.screen.width < 651){
                // 2 ads in a row
                number = 6
            }
            if(window.screen.width > 650 && window.screen.width < 951){
                // 3 ads in a row
                number = 9
            }
            if(window.screen.width > 1500 && window.screen.width < 1951){
                // 5 ads in a row
                number = 15
            }
            if(window.screen.width > 1951){
                // 5 ads in a row
                number = 24
            }
            return number;
        },
        cities() {
            if (store.getters.currentCountry) {

                var cities = [];
                this.all_cities.map(city => {
                    if (city.country_id == store.getters.currentCountry.id) {
                        cities.push(city)
                    }
                });

                return cities;
            } else {
                return [];
            }
        },

    },

    methods: {
        citySearch(options, search) {
            let searched_cities = [];
            options.map(option => {
                if (option.city_name.toLowerCase().search(search) > -1) {
                    searched_cities.push(option);
                }
            })

            if (searched_cities.length == 0) {
                searched_cities = this.cities;
            }
            return searched_cities;
        },
        isValidURL(str) {
            var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
              '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
              '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
              '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
              '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
              '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
            return !!pattern.test(str);
        },
        gotoPage(url) {
            router.push(url)
        }

    },
    data: {
        current_user: '',
        server: 'https://openbazar.nl/app-admin/',
        virtualmin_server: 'https://openbazar.nl/app-admin/',
        // virtualmin_server: 'http://164.90.197.43/app-admin/',
        // virtualmin_server: 'http://main.openbazar.nl/app-admin/',
        country: '',
        local_server: 'http://localhost/open_bazar_api/',
        countries: [
            { id: 1, name: 'Belgium', flag: '/bazar/belgium.png' },
            { id: 2, name: 'France', flag: '/bazar/france.png' },
            { id: 3, name: 'Germany', flag: '/bazar/germany.png' },
            { id: 4, name: 'India', flag: '/bazar/india.png' },
            { id: 5, name: 'Netherlands', flag: '/bazar/netherlands.png' },
            { id: 6, name: 'Poland', flag: '/bazar/poland.png' },
            { id: 7, name: 'Spain', flag: '/bazar/spain.png' },
        ],
        all_cities: store.getters.getAllCities,

    },
    watch: {
        $route(to, from) {
            // console.log(to, from)
            window.scrollTo(0, 0)
        },
    },


});



axios.interceptors.response.use(undefined, function(error) {
    // error.handleGlobally = errorComposer(error);
    console.log(error.response.status)
    if (error.response.status == 401) {
        store.commit('logout')
        toastr.error('Please login to continue');
        // router.push('/');
        window.location.replace('/')
    }

    if (error.response.status == 500) {
        console.log(error.config, 'error.config')
            // return axios.request(error.config);
    }


    // return Promise.reject(error);
})

// const axiosRetry = require('axios-retry');

axios.interceptors.response.use((response) => {
    return response;
}, (error) => {
    if (error.response.status === 500) {
        console.log(error.config, 'error.config')
            // return axios.request(error.config);
    }
    return Promise.reject(error);
});


// axiosRetry(axios, {
//   retries: 3, // number of retries
//   retryDelay: (retryCount) => {
//     console.log(`retry attempt: ${retryCount}`);
//     return retryCount * 2000; // time interval between retries
//   },
//   retryCondition: (error) => {
//     // if retry condition is not specified, by default idempotent requests are retried
//     console.log(error.response , 'error.response axiosRetry')
//     return error.response.status === 500;
//   },
// });


// router.beforeEach((to, from, next) => {
//   console.log(vue.$data , 'VUE')
//   console.log(to.path , vue.$data.current_user , to.path != '/' && vue.$data.current_user)
//   if (to.path != '/' && vue.$data.current_user ) next()
//   else next({ name: 'home' })
//   // next()
// })
