window.Vue = require('vue');
import VueRouter from 'vue-router'
Vue.use(VueRouter)


const routes = [{
        path: '/',
        name: 'home',
        component: () =>
            import ('./public/home.vue')
    },
    {
        path: '/products',
        component: () =>
            import ('./products/products.vue')
    },
    {
        path: '/category/:id/product',
        name: 'category-products',
        component: () =>
            import ('./products/category-products.vue')
    },

    {
        path: '/category-products',
        name: 'Main Category Products',
        props: true,
        component: () =>
            import ('./products/category-products.vue')
    },

    {
        path: '/product/:id/view',
        name: 'view-product',
        props: true,
        component: () =>
            import ('./products/view-product.vue')
    },
    {
        path: '/post-ad',
        name: 'Post Ad',
        component: () =>
            import ('./products/post-ad.vue')
    },
    {
        path: '/edit/:id/ad',
        name: 'Edit Ad',
        component: () =>
            import ('./products/post-ad.vue')
    },


    {
        path: '/help',
        name: 'help',
        component: () =>
            import ('./public/help.vue')
    },
    {
        path: '/contact-us',
        name: 'contact-us',
        component: () =>
            import ('./public/contact-us.vue')
    },
    {
        path: '/terms-of-use',
        name: 'terms-of-use',
        component: () =>
            import ('./public/terms-of-use.vue')
    },
    {
        path: '/privacy-policy',
        name: 'privacy-policy',
        component: () =>
            import ('./public/privacy-policy.vue')
    },



    { path: '/chat', props: true, name: 'chat', component: require('./public/chat.vue').default },
    {
        path: '/notifications',
        component: () =>
            import ('./user/notifications.vue')
    },
    { path: '/payment-method', name: 'payment method', props: true, component: require('./public/payment-methods.vue').default },

    {
        path: '/get-the-app',
        name: 'get-the-app',
        component: () =>
            import ('./public/get-the-app.vue')
    },


    {
        path: '/user/:user_id/ads',
        component: () =>
            import ('./user/other_user/ads.vue')
    },
    {
        path: '/user/:user_id/followers',
        component: () =>
            import ('./user/other_user/followers.vue')
    },
    {
        path: '/user/:user_id/followings',
        component: () =>
            import ('./user/other_user/followings.vue')
    },



    {
        path: '/dashboard/my-profile',
        component: () =>
            import ('./user/dashboard/profile.vue')
    },
    {
        path: '/dashboard/my-wallet',
        component: () =>
            import ('./user/dashboard/wallet.vue')
    },
    {
        path: '/dashboard/my-coins',
        component: () =>
            import ('./user/dashboard/my-coins.vue')
    },
    {
        path: '/dashboard/my-account',
        component: () =>
            import ('./user/dashboard/account.vue')
    },
    {
        path: '/dashboard/my-ads',
        component: () =>
            import ('./user/dashboard/my-ads.vue')
    },
    {
        path: '/dashboard/followings',
        component: () =>
            import ('./user/dashboard/followings.vue')
    },
    {
        path: '/dashboard/followers',
        component: () =>
            import ('./user/dashboard/followers.vue')
    },
    {
        path: '/dashboard/recent-views',
        component: () =>
            import ('./user/dashboard/recent-ads.vue')
    },
    {
        path: '/dashboard/favorite-ads',
        component: () =>
            import ('./user/dashboard/favorite-ads.vue')
    },
    {
        path: '/dashboard/my-offer-ads',
        component: () =>
            import ('./user/dashboard/my-offer-ads.vue')
    },
    {
        path: '/dashboard/following-ads',
        component: () =>
            import ('./user/dashboard/following-ads.vue')
    },
    {
        path: '/dashboard/blocked-users',
        component: () =>
            import ('./user/dashboard/blocked-users.vue')
    },
    {
        path: '/dashboard/invite-freinds',
        component: () =>
            import ('./user/dashboard/invite-freinds.vue')
    },
]



const router = new VueRouter({
    mode: 'history',
    // base: "/openbazar/",
    routes, // short for `routes: routes`
    linkExactActiveClass: "exact-active",
    scrollBehavior (to, from, savedPosition) {
        window.scrollTo(0, 0);
        // document.getElementById('vue').scrollIntoView({ behavior: 'smooth' });
    }

})

const public_routes = [
    'home',
    'help',
    'contact-us',
    'terms-of-use',
    'privacy-policy',
    'get-the-app',
]

router.beforeEach((to, from, next) => {
    // console.log(`beforeEach`, from, to, next)
    // NProgress.start()
    // NProgress.set(0.1)

    // console.log( public_routes.includes(to.name) )
    // if(to.name != 'home' && (!store.getters.isAuthenticated || !store.getters.isAuthenticated.token)   ){
    const store = window.store;
    if (!public_routes.includes(to.name) && (!store.getters.isAuthenticated || !store.getters.isAuthenticated.token)) {

        console.log((!public_routes.includes(to.name) && (!store.getters.isAuthenticated || !store.getters.isAuthenticated.token)), store.getters.isAuthenticated, store.getters.isAuthenticated.token, 'App.js Logout')

        store.commit('setCurrentUrl' , to.path )
        console.log(`currentUrl`, to.path)
        store.commit('logout')
        next('/');
    } else {
        next();
    }
})


export default router;
