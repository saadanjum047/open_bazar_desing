



                    // console.log(this.$store.getters.isAuthenticated);

export default {
    state: {
        search: {
          search: '',
          location: '',
          category: ''
        },
    },
    mutations: {
        searchQuery(state , payload) {

        if(payload.search){
            state.search.search = payload.search;
        }

        if(payload.category){
            state.search.category = payload.category;
        }

        if(payload.location){
            state.search.location = payload.location;
        }

        
        //   console.log(state , payload , 'Store')
      },
      
    },
    getters: {
        fetchSearchQuer(state){
            return state.search;
        }
    },
  }