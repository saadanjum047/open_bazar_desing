const CLASSES = {};

const ERRORS = {
  undefinedValue: 'Value is undefined. This is not valid value!',
};

export { ERRORS, CLASSES };
