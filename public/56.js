(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[56],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/category-products.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/category-products.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _singleProduct_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./singleProduct.vue */ "./resources/js/products/singleProduct.vue");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _singleProductList_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./singleProductList.vue */ "./resources/js/products/singleProductList.vue");
/* harmony import */ var _view_product_social_share_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./view-product/social-share.vue */ "./resources/js/products/view-product/social-share.vue");
/* harmony import */ var _loadingSingleProductList_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./loadingSingleProductList.vue */ "./resources/js/products/loadingSingleProductList.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    'single-product': _singleProduct_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    Loading: vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1___default.a,
    SingleProductList: _singleProductList_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    SocialShare: _view_product_social_share_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
    LoadingSingleProductList: _loadingSingleProductList_vue__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  props: {
    search_query: {
      type: Object
    },
    main_category_id: {}
  },
  data: function data() {
    var _ref;

    return _ref = {
      slider: '',
      main_category: '',
      selected_category: '',
      searched_results: 0,
      city: "",
      // city: this.$root.cities[0],
      type: 'all',
      shareAd: '',
      index: 1,
      search_index: 0,
      sub_categories: [],
      categories: [],
      ads: [],
      loading_categories: false,
      no_more_products: false,
      loading_ads: false,
      loading: false,
      fullPageLoading: false,
      category_changing: false,
      maincategory_id: '',
      main_categories: [{
        id: 1,
        text: 'Sell Categories',
        icon: '/bazar/sell1.png'
      }, {
        id: 2,
        text: 'Rent Categories',
        icon: '/bazar/rent1.png'
      }, {
        id: 3,
        text: 'Services Categories',
        icon: '/bazar/services1.png'
      }, {
        id: 4,
        text: 'Shops Categories',
        icon: '/bazar/shop1.png'
      }],
      country: 'Canada',
      search: ''
    }, _defineProperty(_ref, "main_category", {
      id: 1,
      text: 'Sell Categories',
      icon: '/bazar/sell1.png'
    }), _defineProperty(_ref, "new_filters", {
      sub_categories: [],
      sub_category: '',
      price_range: ['', ''],
      // price_range:{ min: 0 , max: 5000 },
      price_type: [],
      sorted_by: [],
      posted_within: [],
      condition: []
    }), _ref;
  },
  computed: {
    isSearchingNow: function isSearchingNow() {
      if (this.isSearching()) {
        return true;
      } else {
        return false;
      }
    } // isSearching(){
    //     // console.log( this.city.city_name.replace(/\r?\n|\r/g, '') != 'All Cities' , this.city.city_name.replace(/\r?\n|\r/g, '')  , 'isSearching Vales')
    //     console.log('isSearching')
    //     if(this.new_filters.condition.length == 1 || this.new_filters.sorted_by.length == 1 || this.new_filters.price_type.length == 1 || this.selected_category || this.search || this.city.city_name.replace(/\r?\n|\r/g, '') != 'All Cities'){
    //         return true
    //     }else{
    //         // console.log('false')
    //         return false
    //     }
    // }

  },
  destroyed: function destroyed() {
    this.$destroy();
  },
  mounted: function mounted() {
    var _this = this;

    if (this.main_category_id) {
      this.main_categories.map(function (cat) {
        if (cat.id == _this.main_category_id) {
          _this.main_category = cat;
        }
      });
    } else {
      this.main_category = this.main_categories[0];
    } // this.main_categories.map( cat => {
    //     if(cat.id == this.$route.params.id){
    //         this.main_category = cat;
    //     }
    // })


    this.getCategories();
    this.fetchAds(); // console.log(this.$route.params.id)

    this.maincategory_id = this.$route.params.id;
    this.$root.$refs.products = this;
    this.$root.$on('main-category-changed', function (data) {
      // console.log(`main-category-changed`, data)
      _this.main_category = data;

      _this.mainCategoryChanged();
    });
  },
  methods: {
    openAdShareModal: function openAdShareModal(ad) {
      console.log("ad", ad);
      this.shareAd = ad;
      this.$refs.social_modal.openAdShareModal();
    },
    changeAdsToView: function changeAdsToView(view) {
      if (view == 'card') {
        this.$store.commit('setViewOption', 'card');
      } else {
        this.$store.commit('setViewOption', 'list');
      }
    },
    fuseSearch: function fuseSearch(options, search) {
      // console.log(options, search)
      var searched_cities = []; // console.log(options);

      options.map(function (option) {
        // console.log(option);
        // if( option.city_name.search(search) > -1 ){
        //     searched_cities.push(option);
        // }
        if (option.city_name.toLowerCase().search(search) > -1) {
          searched_cities.push(option);
        }
      }); // console.log(searched_cities[0] , searched_cities.length , 'Filtered')

      if (searched_cities.length == 0) {
        searched_cities = this.$root.cities;
      }

      return searched_cities; // return search.length
      //     ? options.map(({ item }) => item)
      //     : options.list;
    },
    searchInitiated: function searchInitiated() {
      var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      console.log('Category Products', query);

      if (query.search) {
        this.search = query.search;
      } else {
        this.search = '';
      }

      if (query.city) {
        this.city = query.city;
      }

      if (query.category) {
        this.main_category = query.category;
      }

      this.searchQuery();
    },
    updateNavbar: function updateNavbar() {
      var search_query = {
        search: this.search,
        city: this.city,
        category: this.main_category
      };

      if (this.$root.$refs.navbar) {
        this.$root.$refs.navbar.updateSearchBar(search_query);
      }
    },
    searchQuery: function searchQuery() {
      this.updateNavbar(); // if(this.search || this.city.city_name != 'All Cities'){

      if (this.isSearching()) {
        // this.searching = true;
        this.searchAds(true);
      } else {
        // this.searching = false;
        this.fetchAds(true);
      }
    },
    refreshProducts: function refreshProducts() {
      this.index = 1;
      this.ads = [];
      this.no_more_products = false;
      this.city = this.$root.cities[0];
      this.fetchAds();
    },
    fetchAds: function fetchAds() {
      var _this2 = this;

      var reset = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      console.log('fetchAds', reset, this.no_more_products, this.isSearching());
      this.updateNavbar();

      if (reset) {
        this.ads = [];
        this.fullPageLoading = true;
        this.no_more_products = false;
        this.index = 1;
        window.scrollTo(0, 0);
      }

      this.loading = true;
      axios.post('/get_all_post', {
        type: this.main_category.id,
        user_id: this.$store.getters.isAuthenticated.id,
        country_id: this.$store.getters.currentCountry.id,
        index: this.index
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        // console.log(res.data)
        _this2.loading = false;
        _this2.fullPageLoading = false;

        if (res.data.status == 'success') {
          var _this2$ads;

          (_this2$ads = _this2.ads).push.apply(_this2$ads, _toConsumableArray(res.data.data));

          _this2.index = _this2.index + 1;
        } else if (res.data.status == 'fail') {
          _this2.no_more_products = true;
        } else {
          console.log(res);

          _this2.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this2.loading = false;
        _this2.fullPageLoading = false;

        _this2.$toastr.error('An unexpected error occurred');
      });
    },
    searchAds: function searchAds() {
      var _this3 = this;

      var reset = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (reset) {
        this.ads = [];
        this.fullPageLoading = true;
        this.no_more_products = false;
        this.index = 0;
      }

      this.loading = true;
      axios.post('/v1/search_ads', {
        category: this.main_category.id,
        sub_sub_category: this.new_filters && this.new_filters.sub_category ? this.new_filters.sub_category.id : "",
        cat_id: this.selected_category.id,
        search: this.search,
        city: this.city.city_name,
        // index: this.index,
        country_id: this.$store.getters.currentCountry.id,
        price_min: this.new_filters.price_range ? this.new_filters.price_range[0] : '',
        price_max: this.new_filters.price_range ? this.new_filters.price_range[1] : '',
        // sub_category: this.new_filters.sub_category ? this.new_filters.sub_category.id : '',
        price_type: this.new_filters.price_type.length ? this.new_filters.price_type[0] : '',
        condition: this.new_filters.condition.length ? this.new_filters.condition[0] : '',
        sort: this.new_filters.sorted_by.length ? this.new_filters.sorted_by[0] : ''
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        // console.log(res.data)
        _this3.loading = false;
        _this3.fullPageLoading = false;

        if (res.data.status == 'true') {
          _this3.ads = res.data.data;
          window.scrollTo(0, 0);
          _this3.searched_results = res.data.data.length; // this.index = this.index + 1
        } else if (res.data.status == 'false') {
          _this3.no_more_products = true;
        } else {
          console.log(res);

          _this3.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this3.loading = false;
        _this3.fullPageLoading = false;

        _this3.$toastr.error('An unexpected error occurred');
      });
    },
    getCategories: function getCategories() {
      var _this4 = this;

      this.loading_categories = true;
      this.loading = true;
      console.log(this.maincategory_id);
      axios.post('/get_category', {
        maincategory_id: this.main_category.id
      }).then(function (res) {
        var initial_fetch = true;

        if (_this4.categories.length > 0) {
          initial_fetch = false;
        }

        _this4.categories = res.data.data; // console.log(res.data.data)
        // this.selected_category = this.categories[0];
        // this.getSubCategories();

        if (_this4.slider) {
          _this4.slider.destroy();
        }

        _this4.initializeSelect2(); // if(initial_fetch){
        //     this.initializeSelect2();
        // }else{
        //     this.loading_categories = false;
        // }
        // console.log(this.slider , 'this.slider.refresh()')
        // this.getCategoryPorducts();

      })["catch"](function (err) {
        _this4.loading_categories = false;
        console.log(err);
      });
    },
    getSubCategories: function getSubCategories() {
      var _this5 = this;

      this.category_changing = true;
      axios.post('/get_subcategory', {
        id: this.selected_category.id
      }).then(function (res) {
        _this5.category_changing = false;
        _this5.sub_categories = res.data.data;

        _this5.$root.$emit('sub-category-selected');
      })["catch"](function (err) {
        _this5.category_changing = false;
        console.log(err);
      });
    },
    loadMoreAds: function loadMoreAds() {
      // this.index += 1;
      if (!this.loading) {
        if (!this.isSearching()) {
          this.fetchAds();
        } else {
          this.searchAds();
        }
      }
    },
    pageEnd: function pageEnd(msg) {
      console.log(msg);
    },
    getCategoryPorducts: function getCategoryPorducts() {
      var _this6 = this;

      this.loading_ads = true;
      this.loading = true;
      axios.post('/get_productcat_new', {
        // id: this.category_id
        id: 12
      }).then(function (res) {
        _this6.loading_ads = false;
        _this6.loading = false;
        _this6.ads = res.data.data;
      })["catch"](function (err) {
        _this6.loading_ads = false;
        _this6.loading = false;
        console.log(err);
      });
    },
    updateCategory: function updateCategory(category) {
      this.selected_category = category;
      this.new_filters.sub_category = '';
      this.getSubCategories();
      this.searchAds(true);
    },
    // getCategoryPorducts(){
    //     // http://localhost/open_bazar_api/api/v1/get_subcategory
    //     this.loading_ads = true;
    //     axios.post('/v1/get_productcat_new' , {
    //         id: this.sub_category_id
    //     })
    //     .then (res => {
    //         this.loading_ads = false;
    //         this.ads = res.data.data
    //     }).catch(err => {
    //         this.loading_ads = false;
    //         console.log(err)
    //     })
    // },
    filtersUpdate: function filtersUpdate($filter, $val) {
      this.filters[$filter] = !this.filters[$val]; // this.filters.condition.old = !this.filters.condition.old
      // console.log($filter , $val);
    },
    filtersUpdateNew: function filtersUpdateNew(filter) {
      // console.log(this.isSearching() , 'isSearching');
      // console.log(filter , 'PARENT' , this.new_filters[filter] , this.new_filters[filter] && this.isSearching  , this.isSearching)
      if (this.new_filters.price_type && this.new_filters.price_type[0] != 'price') {
        this.new_filters.price_range[0] = '';
        this.new_filters.price_range[1] = '';
      }

      if (this.new_filters[filter] && this.isSearching()) {
        $('#searchFilterModal').modal('hide');
        this.searchAds(true);
      } else {
        this.filtersCleared();
      }
    },
    filtersCleared: function filtersCleared() {
      this.new_filters.condition = [];
      this.new_filters.price_type = [];
      this.new_filters.sorted_by = [];
      this.new_filters.price_range[0] = '';
      this.new_filters.price_range[1] = '';
      this.new_filters.sub_category = '';
      this.selected_category = '';
      this.city = '';
      this.search = '';
      this.isSearching();
      this.$root.$emit('category-filters-reset');
      $('#searchFilterModal').modal('hide');
      this.fetchAds(true); // this.new_filters.condition = []
    },
    validFilters: function validFilters() {
      if (this.filters.price_range.min || this.filters.price_range.max || this.filters.sorted_by.urgent || this.filters.sorted_by.popular || this.filters.sorted_by.low_to_high || this.filters.sorted_by.high_to_low || this.filters.condition["new"] || this.filters.condition.old || this.filters.condition.used_like_new) {
        return true;
      } else {
        return false;
      }
    },
    mainCategoryChanged: function mainCategoryChanged() {
      this.getCategories();
      this.selected_category = '';
      this.fetchAds(true);
    },
    isSearching: function isSearching() {
      // console.log(this.new_filters.sorted_by)
      if (this.new_filters.condition.length == 1 || this.new_filters.sorted_by.length == 1 || this.new_filters.price_type.length == 1 || this.selected_category || this.search || this.city && this.city.city_name.replace(/\r?\n|\r/g, '') != 'All Cities') {
        // console.log('true' , 'isSearching' )
        return true;
      } else {
        // console.log('false' , 'isSearching')
        return false;
      }
    },
    openSearchModal: function openSearchModal() {
      $('#searchFilterModal').modal('show');
    },
    initializeSelect2: function initializeSelect2() {
      var _this7 = this;

      $(document).ready(function () {
        // $('.flexslider').flexslider({
        //     controlsContainer: '.flexslider'
        // });
        _this7.slider = $('#responsive'); // slider.destroy();

        _this7.slider.lightSlider({
          item: 0,
          loop: false,
          //slideMove:2,
          easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
          speed: 600,
          controls: false,
          slideMargin: 33,
          pager: false,
          autoWidth: true,
          enableTouch: true,
          enableDrag: true //touchDrag: true,

        });

        $('#goToPrevSlide').on('click', function () {
          _this7.slider.goToPrevSlide();
        });
        $('#goToNextSlide').on('click', function () {
          _this7.slider.goToNextSlide();
        }); // console.log($('#responsive').length , 'Slider Active')

        _this7.loading_categories = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/loadingSingleProductList.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/loadingSingleProductList.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/category-products.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/category-products.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.category-products-banner-top{\n        max-height: 90px !important;\n        height: 90px !important;\n        width: 100%;\n        max-width: 100%;\n        text-align: center !important;\n}\n.category-ads-in-feed-banners{\n        /* background-color: red; */\n        max-height: 120px !important;\n        height: 120px !important;\n        width: 100%;\n        padding: 0 4px;\n        max-width: 100%;\n        text-align: center !important;\n}\n@media screen and (max-width:550px) {\n.category-ads-in-feed-banners{\n            max-height: 320px !important;\n            height: 320px !important;\n}\n}\n.product-box-ad{\n        width: 100%;\n        height: 100;\n        max-height:385px !important;\n}\n    /* .v-select-in-categories .vs__dropdown-toggle{\n        padding: 3px 0 2px;\n        border: 1px solid #d11d1d;\n        border-radius: unset;\n        border-right: none;\n    }\n    .vs__selected-options{\n        margin-top: 0;\n        font-size: 16px;\n    }\n\n    .vs__dropdown-option .vs__selected{\n        color: #4e6a80;\n    }\n    .vs__dropdown-option--highlight{\n        color: #4e6a80 !important;\n        background-color: #cccccc;\n    }\n    .vs__dropdown-option{\n        padding: 8px 5px !important;\n    } */\n\n\n    /* @media screen and (max-width:786px){\n        .v-select-in-categories .vs__dropdown-toggle{\n            border-radius: 10px;\n            border: none;\n            background-color: white;\n            height: 44px;\n            margin-bottom: 12px;\n        }\n\n        .v-select-for-location .vs__selected .country-icon img{\n            margin-left: 7px;\n        }\n    } */\n\n/*\n    .v-select-for-location .vs__selected .country-icon img{\n        width: 14px;\n        margin-right: 2px;\n        margin-top: -4px;\n\n    }\n    .v-select-for-location .vs__selected .country-icon span{\n        margin-top: 2px !important;\n        display: inline-block;\n    }\n\n    .v-select-for-location .vs__selected{\n        color: #4e6a80;\n        margin: 0 !important;\n    }\n    .v-select-for-location .vs__dropdown-option .vs__selected{\n        color: #4e6a80;\n        margin-left: 20px !important;\n    }\n    .v-select-for-location .select-open-indicator{\n        margin-right: 15px;\n        margin-top: -5px;\n    }\n    .v-select-for-location .select-open-indicator img{\n        width: 12px;\n    }\n\n    .v-select-for-location .option-icon{\n        width: 25px;\n    }\n    .v-select-for-categories .vs__dropdown-menu li{\n        padding: 5px 3px;\n        color: #4e6a80;\n    }\n    .v-select-for-categories .vs__dropdown-menu li img{\n        margin-right: 15px;\n    }\n    .fixed-searh-bar .search-bar .searh-bar-conatiner .select-2-filter-box{\n        width: 18% !important;\n    }\n\n    .v-select-for-categories .vs__selected .country-icon{\n        display: flex;\n        justify-content: center;\n        align-items: center;\n    }\n    .v-select-for-categories .vs__selected .country-icon img{\n        width: 25px !important;\n        margin-right: 3px;\n    }\n    .v-select-for-categories .vs__selected .category-icon-container{\n        width: 100% !important;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n    }\n    .v-select-for-categories .vs__selected .category-icon-container img{\n        width: 25px !important;\n        margin-top: -2px;\n    }\n    .vs--single.vs--open .vs__selected{\n        opacity: 1 !important;\n    }\n    .vs__dropdown-menu{\n        background-color: #fafafa !important;\n    } */\n.lSSlideOuter {\n        padding: 0 20px !important;\n}\n@media screen and (max-width:786px){\n.lSSlideOuter{\n            padding: 0 20px !important;\n}\n}\n.filter-checkbox{\n        position: relative;\n}\n.check-filter{\n        cursor: pointer;\n}\n.filter-checkbox {\n        display: block;\n        position: relative;\n        padding-left: 30px;\n        margin-bottom: 5px;\n        cursor: pointer;\n        -webkit-user-select: none;\n        -moz-user-select: none;\n        -ms-user-select: none;\n        user-select: none;\n}\n\n      /* Hide the browser's default checkbox */\n.filter-checkbox input {\n        position: absolute;\n        opacity: 0;\n        cursor: pointer;\n        height: 0;\n        width: 0;\n}\n\n      /* Create a custom checkbox */\n.checkmark {\n        position: absolute;\n        top: 0;\n        left: 0;\n        height: 20px;\n        width: 20px;\n        background-color: #eee;\n}\n\n      /* On mouse-over, add a grey background color */\n.check-filter:hover input ~ .checkmark {\n        background-color: #ccc;\n        border-radius: 5px;\n}\n.filter-checkbox input ~ .checkmark {\n        border-radius: 5px;\n}\n\n      /* When the checkbox is checked, add a blue background */\n.filter-checkbox input:checked ~ .checkmark {\n        background-color: #d11d1d;\n}\n\n      /* Create the checkmark/indicator (hidden when not checked) */\n.checkmark:after {\n        content: \"\";\n        position: absolute;\n        display: none;\n}\n\n      /* Show the checkmark when checked */\n.filter-checkbox input:checked ~ .checkmark:after {\n        display: block;\n}\n\n      /* Style the checkmark/indicator */\n.filter-checkbox .checkmark:after {\n        left: 7px;\n        top: 4px;\n        width: 6px;\n        height: 10px;\n        border: solid white;\n        border-width: 0 3px 3px 0;\n        transform: rotate(45deg);\n}\n.irs-line{\n            height: 4px !important;\n            cursor: pointer;\n}\n.irs--flat .irs-bar{\n        height: 4px !important;\n        background-color: #d11d1d !important;\n}\n.irs--flat .irs-handle>i:first-child{\n        width: 15px !important;\n        border-radius: 200px !important;\n        height: 80% !important;\n        background-color: #d11d1d !important;\n        cursor: pointer;\n        left: 2px !important;\n}\n      /* .irs--flat .irs-handle:nth-child(1){\n          left: -10px !important;\n      } */\n.irs--flat .irs-handle{\n        top: 20px !important;\n}\n.irs-from{\n        background-color: #d11d1d !important;\n}\n.irs-to{\n        background-color: #d11d1d !important;\n}\n.irs-single{\n        background-color: #d11d1d !important;\n}\n.modal-header{\n          border-bottom: unset;\n          padding: 10px;\n}\n.modal-body{\n          padding: 10px;\n}\n.modal-close-button{\n        position: absolute;\n        left: 0;\n        top: 10px;\n}\n.sticky-container{\n        margin-bottom: 200px;\n}\n.fixed-container{\n        position: fixed;\n        bottom: 10px;\n        margin-bottom: 200px;\n}\n.fixed-top {\n        position: absolute;\n        top: 10px;\n}\n.fixed-bottom {\n        position: absolute;\n        bottom: 10px;\n}\n.price-range-input{\n        width: 45%;\n        border-radius: 3px;\n        border: 1px solid #c0c0c0ad;\n        text-align: center;\n        outline: none;\n}\n.box {\n        width: 227px;\n        height: 227px;\n        background-color: #f6f6f6;\n}\n.box a {\n        color: #461e52;\n        display: block;\n        width: 100%;\n        height: 100%;\n        font-size: 22px;\n        font-weight: 700;\n        padding: 30% 30px 0 30px;\n        text-align: center;\n        transition: none;\n        line-height: 1;\n        font-family: 'Open Sans Condensed', Arial, Verdana, sans-serif;\n}\n.box:hover{\n        background-color: #461e52;\n}\n.box:hover a{\n       color:#fff;\n       text-decoration:none;\n}\n.mission-next-arrow {\n        position: absolute;\n        background: url(https://raw.githubusercontent.com/solodev/icon-box-slider/master/nextarrow2.png) no-repeat center;\n        background-size: contain;\n        top: 50%;\n        transform: translateY(-50%);\n        right: -36px;\n        height: 17px;\n        width: 10px;\n        border:none;\n}\n.mission-next-arrow:hover {\n        cursor: pointer;\n}\n.mission-prev-arrow {\n        background: url(https://raw.githubusercontent.com/solodev/icon-box-slider/master/prevarrow2.png) no-repeat center;\n        background-size: contain;\n        position: absolute;\n        top: 50%;\n        transform: translateY(-50%);\n        left: -36px;\n        height: 17px;\n        width: 10px;\n        border:none;\n}\n.mission-prev-arrow:hover {\n        cursor: pointer;\n}\n.box a.more-links {\n        color: #fff;\n        padding: 70px 110px 0 20px;\n        background: #a89269 url(https://raw.githubusercontent.com/solodev/icon-box-slider/master/rightarrow.png) no-repeat 155px 170px;\n}\n.slick-item{\n          height: 50px;\n          width: 50px;\n          background-color: white;\n          margin: 20px;\n}\n.splide__slide{\n          visibility: hidden;\n          transition: all .1s ease-in-out;\n}\n.is-visible{\n          visibility: visible;\n}\n.category-ads-banner-vertical-container{\n        /* margin-top: 32px; */\n}\n.category-ads-banner-vertical{\n        width: 100%;\n        max-width: 100%;\n}\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/category-products.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/category-products.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./category-products.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/category-products.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/category-products.vue?vue&type=template&id=5a611b34&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/category-products.vue?vue&type=template&id=5a611b34& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "content category-products" },
    [
      _c("loading", {
        attrs: {
          active: _vm.fullPageLoading,
          loader: "dots",
          height: 170,
          width: 170,
          color: "#d11d1d",
          "background-color": "rgb(0 0 0)",
          "lock-scroll": true,
          "is-full-page": true
        },
        on: {
          "update:active": function($event) {
            _vm.fullPageLoading = $event
          }
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "search-bar" }, [
        _c("div", { staticClass: "searh-bar-conatiner" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.search,
                expression: "search"
              }
            ],
            staticClass: "search-filter",
            attrs: { placeholder: "What are you looking for... " },
            domProps: { value: _vm.search },
            on: {
              keydown: function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                ) {
                  return null
                }
                return _vm.searchQuery($event)
              },
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.search = $event.target.value
              }
            }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "select-2-filter-box category-dropdown" },
            [
              _c("v-select", {
                staticClass:
                  "v-select-for-location v-select-in-categories v-select-for-categories",
                attrs: {
                  clearable: false,
                  searchable: false,
                  options: _vm.main_categories,
                  label: "text"
                },
                on: { input: _vm.mainCategoryChanged },
                scopedSlots: _vm._u([
                  {
                    key: "open-indicator",
                    fn: function(ref) {
                      var attributes = ref.attributes
                      return [
                        _c(
                          "span",
                          _vm._b(
                            { staticClass: "select-open-indicator" },
                            "span",
                            attributes,
                            false
                          ),
                          [
                            _c("img", {
                              attrs: { src: "/bazar/down-arrow (1).png" }
                            })
                          ]
                        )
                      ]
                    }
                  },
                  {
                    key: "selected-option",
                    fn: function(ref) {
                      var text = ref.text
                      var icon = ref.icon
                      return [
                        _c("div", { staticClass: "category-icon-container" }, [
                          _c("img", { attrs: { src: icon } }),
                          _vm._v(" "),
                          _c("span", [
                            _vm._v(
                              "\n                                    " +
                                _vm._s(text) +
                                "\n                                "
                            )
                          ])
                        ])
                      ]
                    }
                  },
                  {
                    key: "option",
                    fn: function(ref) {
                      var text = ref.text
                      var icon = ref.icon
                      return [
                        _c("div", { staticClass: "d-flex align-item-center" }, [
                          _c("img", {
                            staticClass: "option-icon",
                            attrs: { src: icon }
                          }),
                          _vm._v(" "),
                          _c("span", { staticClass: "vs__selected" }, [
                            _vm._v(_vm._s(text))
                          ])
                        ])
                      ]
                    }
                  }
                ]),
                model: {
                  value: _vm.main_category,
                  callback: function($$v) {
                    _vm.main_category = $$v
                  },
                  expression: "main_category"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "select-2-filter-box" },
            [
              _vm._m(0),
              _vm._v(" "),
              _c("v-select", {
                staticClass:
                  "v-select-for-location v-select-in-categories v-select-location-only v-select-location--cateogry bg-white",
                attrs: {
                  filter: _vm.$root.citySearch,
                  clearable: false,
                  options: _vm.$root.cities,
                  label: "city_name",
                  placeholder: "Select City"
                },
                on: {
                  input: _vm.searchQuery,
                  keydown: function($event) {
                    if (
                      !$event.type.indexOf("key") &&
                      _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                    ) {
                      return null
                    }
                    return _vm.searchQuery($event)
                  }
                },
                scopedSlots: _vm._u([
                  {
                    key: "open-indicator",
                    fn: function(ref) {
                      var attributes = ref.attributes
                      return [
                        _c(
                          "span",
                          _vm._b(
                            { staticClass: "select-open-indicator" },
                            "span",
                            attributes,
                            false
                          ),
                          [
                            _c("img", {
                              attrs: { src: "/bazar/down-arrow (1).png" }
                            })
                          ]
                        )
                      ]
                    }
                  }
                ]),
                model: {
                  value: _vm.city,
                  callback: function($$v) {
                    _vm.city = $$v
                  },
                  expression: "city"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "search-filter search-filter-button",
              on: { click: _vm.searchQuery }
            },
            [_vm._v("SEARCH ")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "w-100 search-filter-sm" }, [
            _c(
              "button",
              {
                staticClass: "search-filter filter-modal-button",
                on: { click: _vm.openSearchModal }
              },
              [_vm._v("FILTER MENU")]
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "search-button-sm",
                on: { click: _vm.searchQuery }
              },
              [_vm._v("SEARCH")]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "container-fluid container-box" }, [
        _c("div", { staticClass: "row" }, [
          _c(
            "div",
            {
              staticClass: "col-xs-12 col-sm-12 col-md-12 col-lg-2 filter-cols"
            },
            [
              _c(
                "div",
                { staticClass: "filter-sidebar filters-desk" },
                [
                  _c("sidebar-filters-new", {
                    ref: "sidebar_filters",
                    attrs: {
                      categories: _vm.sub_categories,
                      category: _vm.selected_category,
                      isSearching: _vm.isSearchingNow,
                      category_changing: _vm.category_changing,
                      filters: _vm.new_filters
                    },
                    on: {
                      "filters-update": _vm.filtersUpdateNew,
                      "filters-cleared": _vm.filtersCleared
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "sticky-container" }, [
                _c(
                  "div",
                  { staticClass: "side-bar-filters qr-code-container" },
                  [
                    _c("div", { staticClass: "heading" }, [
                      _vm._v("Open Bazar mobile App")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "sub-heading" }, [
                      _vm._v("Search Anytime Anywhere")
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "barcode-container" },
                      [
                        _c("router-link", { attrs: { to: "/get-the-app" } }, [
                          _c("img", {
                            attrs: {
                              id: "barcode",
                              src: "/bazar/latest-qr.png",
                              alt: "",
                              title: "HELLO"
                            }
                          })
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "download-statement" },
                      [
                        _c("div", [_vm._v("Scan or click to")]),
                        _vm._v(" "),
                        _c(
                          "router-link",
                          {
                            staticClass: "download-text cursor-pointer",
                            attrs: { to: "/get-the-app" }
                          },
                          [_vm._v("Download")]
                        )
                      ],
                      1
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass:
                      "googe-adsense-banner category-ads-banner-vertical-container"
                  },
                  [
                    _c("Adsense", {
                      staticStyle: {
                        border: "0pt none",
                        display: "inline-block",
                        width: "100%"
                      },
                      attrs: {
                        "data-ad-client": "ca-pub-6933751435930860",
                        "data-ad-slot": "6003562808",
                        "ins-class": "category-ads-banner-vertical",
                        "data-ad-format": "auto",
                        "data-full-width-responsive": "true"
                      }
                    })
                  ],
                  1
                )
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass:
                "col-xs-12 col-sm-12 col-md-12 col-lg-10  p-0 m-0 products-container"
            },
            [
              _c(
                "div",
                { staticClass: "col-12 p-0 m-0" },
                [
                  _c("Adsense", {
                    staticStyle: {
                      border: "0pt none",
                      display: "inline-block",
                      width: "100%",
                      height: "90px"
                    },
                    attrs: {
                      "data-ad-client": "ca-pub-6933751435930860",
                      "data-ad-slot": "3502835629",
                      "ins-class": "category-products-banner-top",
                      "is-new-ads-code": "yes"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "categories-slider" },
                    [
                      _vm.loading_categories
                        ? _c("loader", { staticClass: "bz-text-red" })
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "ul",
                        { attrs: { id: "responsive" } },
                        _vm._l(_vm.categories, function(category, index) {
                          return _c(
                            "li",
                            {
                              key: index,
                              class: {
                                "active-category":
                                  category == _vm.selected_category
                              },
                              on: {
                                click: function($event) {
                                  return _vm.updateCategory(category)
                                }
                              }
                            },
                            [
                              _c(
                                "div",
                                { staticClass: "slider-category-item" },
                                [
                                  _c("img", {
                                    attrs: {
                                      src:
                                        _vm.$root.virtualmin_server +
                                        "public/images/" +
                                        category.image
                                    }
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "slider-category-name" },
                                [_vm._v(_vm._s(category.en_name.substr(0, 15)))]
                              )
                            ]
                          )
                        }),
                        0
                      ),
                      _vm._v(" "),
                      _vm._m(1),
                      _vm._v(" "),
                      _vm._m(2)
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-12 p-0 m-0" }, [
                    !_vm.loading && _vm.ads.length == 0
                      ? _c("div", { staticClass: "no-ads-message" }, [
                          _vm._v("No Ads Found")
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.$store.getters.getViewOption == "card"
                      ? _c(
                          "div",
                          {
                            staticClass: "product-section",
                            attrs: { id: "product-section" }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "product-list" },
                              [
                                _vm._l(_vm.ads, function(ad, index) {
                                  return [
                                    index > 0 &&
                                    index % _vm.$root.numberOfAdsForAdsense ===
                                      0
                                      ? _c("Adsense", {
                                          key: index * index + index,
                                          staticStyle: {
                                            border: "0pt none",
                                            display: "inline-block",
                                            width: "100%"
                                          },
                                          attrs: {
                                            "data-ad-client":
                                              "ca-pub-6933751435930860",
                                            "data-ad-slot": "6003562808",
                                            "is-new-ads-code": "yes",
                                            "ins-class":
                                              "category-ads-in-feed-banners"
                                          }
                                        })
                                      : _vm._e(),
                                    _vm._v(" "),
                                    _c("single-product", {
                                      key: ad.id,
                                      attrs: {
                                        no_more_products: _vm.no_more_products,
                                        searching: _vm.isSearchingNow,
                                        index: index,
                                        ad: ad
                                      },
                                      on: { "load-more-ads": _vm.loadMoreAds }
                                    })
                                  ]
                                }),
                                _vm._v(" "),
                                _vm._l(8, function(index) {
                                  return _c("loading-single-product", {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.loading,
                                        expression: "loading"
                                      }
                                    ],
                                    key: index
                                  })
                                })
                              ],
                              2
                            )
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.$store.getters.getViewOption == "list"
                      ? _c(
                          "div",
                          {
                            staticClass: "horizontal-product-section",
                            attrs: { id: "horizontal-product-section" }
                          },
                          [
                            _c("social-share", {
                              ref: "social_modal",
                              attrs: { ad: _vm.shareAd }
                            }),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "horizontal-product-list" },
                              [
                                _vm._l(_vm.ads, function(ad, index) {
                                  return _c("single-product-list", {
                                    key: ad.id,
                                    attrs: {
                                      no_more_products: _vm.no_more_products,
                                      searching: _vm.isSearchingNow,
                                      index: index,
                                      ad: ad
                                    },
                                    on: {
                                      "show-share-dialog-box":
                                        _vm.openAdShareModal,
                                      "load-more-ads": _vm.loadMoreAds
                                    }
                                  })
                                }),
                                _vm._v(" "),
                                _vm._l(8, function(index) {
                                  return _c("loading-single-product-list", {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.loading,
                                        expression: "loading"
                                      }
                                    ],
                                    key: index
                                  })
                                })
                              ],
                              2
                            )
                          ],
                          1
                        )
                      : _vm._e()
                  ])
                ],
                1
              )
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "searchFilterModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalCenterTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(3),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c(
                    "div",
                    { staticClass: "filters-mobile" },
                    [
                      _c("sidebar-filters-new", {
                        ref: "sidebar_filters",
                        attrs: {
                          categories: _vm.sub_categories,
                          isSearching: _vm.isSearching,
                          category: _vm.selected_category,
                          category_changing: _vm.category_changing,
                          filters: _vm.new_filters
                        },
                        on: { "filters-update": _vm.filtersUpdateNew }
                      })
                    ],
                    1
                  )
                ])
              ])
            ]
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "abs-location-icon-category-products" }, [
      _c("img", { attrs: { src: "/bazar/location.png" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "custom-slider-control",
        attrs: { type: "button", id: "goToPrevSlide" }
      },
      [_c("i", { staticClass: "fa fa-angle-left" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "custom-slider-control",
        attrs: { type: "button", id: "goToNextSlide" }
      },
      [_c("i", { staticClass: "fa fa-angle-right" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close modal-close-button",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/loadingSingleProductList.vue?vue&type=template&id=16eff0a6&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/loadingSingleProductList.vue?vue&type=template&id=16eff0a6& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "product-box" }, [
    _c("div", { staticClass: "product" }, [
      _c("a", { attrs: { href: "#" } }, [
        _c(
          "div",
          { staticClass: "feature-image" },
          [
            _c("b-skeleton-img", { attrs: { "no-aspect": "", height: "100%" } })
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "loading-product-details pb-3" },
        [
          _c("b-skeleton", { attrs: { animation: "wave", width: "85%" } }),
          _vm._v(" "),
          _c("b-skeleton", { attrs: { animation: "wave", width: "55%" } }),
          _vm._v(" "),
          _c("b-skeleton", { attrs: { animation: "wave", width: "70%" } }),
          _vm._v(" "),
          _c("b-skeleton", { attrs: { animation: "wave", width: "85%" } }),
          _vm._v(" "),
          _c("b-skeleton", { attrs: { animation: "wave", width: "55%" } }),
          _vm._v(" "),
          _c("b-skeleton", { attrs: { animation: "wave", width: "70%" } }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "d-flex justify-center mt-5 w-100" },
            [
              _c("b-skeleton", { attrs: { type: "button" } }),
              _vm._v(" "),
              _c("b-skeleton", { attrs: { type: "button" } }),
              _vm._v(" "),
              _c("b-skeleton", { attrs: { type: "button" } }),
              _vm._v(" "),
              _c("b-skeleton", { attrs: { type: "button" } })
            ],
            1
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/products/category-products.vue":
/*!*****************************************************!*\
  !*** ./resources/js/products/category-products.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _category_products_vue_vue_type_template_id_5a611b34___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-products.vue?vue&type=template&id=5a611b34& */ "./resources/js/products/category-products.vue?vue&type=template&id=5a611b34&");
/* harmony import */ var _category_products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./category-products.vue?vue&type=script&lang=js& */ "./resources/js/products/category-products.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _category_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./category-products.vue?vue&type=style&index=0&lang=css& */ "./resources/js/products/category-products.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _category_products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _category_products_vue_vue_type_template_id_5a611b34___WEBPACK_IMPORTED_MODULE_0__["render"],
  _category_products_vue_vue_type_template_id_5a611b34___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/products/category-products.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/products/category-products.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/products/category-products.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_category_products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./category-products.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/category-products.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_category_products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/products/category-products.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************!*\
  !*** ./resources/js/products/category-products.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_category_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./category-products.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/category-products.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_category_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_category_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_category_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_category_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/products/category-products.vue?vue&type=template&id=5a611b34&":
/*!************************************************************************************!*\
  !*** ./resources/js/products/category-products.vue?vue&type=template&id=5a611b34& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_category_products_vue_vue_type_template_id_5a611b34___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./category-products.vue?vue&type=template&id=5a611b34& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/category-products.vue?vue&type=template&id=5a611b34&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_category_products_vue_vue_type_template_id_5a611b34___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_category_products_vue_vue_type_template_id_5a611b34___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/products/loadingSingleProductList.vue":
/*!************************************************************!*\
  !*** ./resources/js/products/loadingSingleProductList.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _loadingSingleProductList_vue_vue_type_template_id_16eff0a6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loadingSingleProductList.vue?vue&type=template&id=16eff0a6& */ "./resources/js/products/loadingSingleProductList.vue?vue&type=template&id=16eff0a6&");
/* harmony import */ var _loadingSingleProductList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./loadingSingleProductList.vue?vue&type=script&lang=js& */ "./resources/js/products/loadingSingleProductList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _loadingSingleProductList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _loadingSingleProductList_vue_vue_type_template_id_16eff0a6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _loadingSingleProductList_vue_vue_type_template_id_16eff0a6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/products/loadingSingleProductList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/products/loadingSingleProductList.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/products/loadingSingleProductList.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleProductList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./loadingSingleProductList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/loadingSingleProductList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleProductList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/products/loadingSingleProductList.vue?vue&type=template&id=16eff0a6&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/products/loadingSingleProductList.vue?vue&type=template&id=16eff0a6& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleProductList_vue_vue_type_template_id_16eff0a6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./loadingSingleProductList.vue?vue&type=template&id=16eff0a6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/loadingSingleProductList.vue?vue&type=template&id=16eff0a6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleProductList_vue_vue_type_template_id_16eff0a6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleProductList_vue_vue_type_template_id_16eff0a6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);