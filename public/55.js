(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[55],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/public/help.vue?vue&type=template&id=348c7499&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/public/help.vue?vue&type=template&id=348c7499& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container help-page mt-5" }, [
      _c("div", { staticClass: "bz-dashboard-page-header" }, [
        _c("div", { staticClass: "bz-page-header-title text-center" }, [
          _vm._v("\n            Help\n        ")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "list mt-5 mb-5" }, [
        _c("div", { staticClass: "list-item" }, [
          _c(
            "a",
            {
              staticClass: "accordian-link collapsed",
              attrs: {
                href: "#",
                "data-toggle": "collapse",
                "data-target": "#accordian-1"
              }
            },
            [
              _c("div", { staticClass: "list-item-name" }, [
                _vm._v("\n            What is the Open Bazar? \n        ")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "list-item-icon" }, [
                _c("i", { staticClass: "fas fa-angle-down arrow-down-right" })
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "collapse", attrs: { id: "accordian-1" } }, [
          _c("div", { staticClass: "expandable-area" }, [
            _vm._v(
              "\n            The Open Bazar is an online market specialized in classified ads that allow the user to add special ads or browse the existing ads. That facilitates and accelerates the business processes of selling and buying and renting of many new and used things. As well as services according to the classification on the lists on the site to be connected between the advertiser and browser from the same country or the city is direct and fast.\n        "
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "list-item" }, [
          _c(
            "a",
            {
              staticClass: "accordian-link collapsed",
              attrs: {
                href: "#",
                "data-toggle": "collapse",
                "data-target": "#accordian-2"
              }
            },
            [
              _c("div", { staticClass: "list-item-name" }, [
                _vm._v("\n            How does the site work? \n        ")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "list-item-icon" }, [
                _c("i", { staticClass: "fas fa-angle-down arrow-down-right" })
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "collapse", attrs: { id: "accordian-2" } }, [
          _c("div", { staticClass: "expandable-area" }, [
            _vm._v(
              "\n            The Open Bazar is an online market specialized in classified ads that allow the user to add special ads or browsThe Open Bazar is a safe and easy to use platform for the sale and purchase of real estate, vehicles, electronics, furniture, telephones, clothing and many other supplies, in addition to services.\n        "
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "list-item" }, [
          _c(
            "a",
            {
              staticClass: "accordian-link collapsed",
              attrs: {
                href: "#",
                "data-toggle": "collapse",
                "data-target": "#accordian-3"
              }
            },
            [
              _c("div", { staticClass: "list-item-name" }, [
                _vm._v(
                  "\n            Is the Open Bazar subscription free? \n        "
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "list-item-icon" }, [
                _c("i", { staticClass: "fas fa-angle-down arrow-down-right" })
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "collapse", attrs: { id: "accordian-3" } }, [
          _c("div", { staticClass: "expandable-area" }, [
            _vm._v(
              "\n            Yes, the Open Bazar is a free site with the possibility of subscription paid services that provide the user with many advantages for a small fee.\n        "
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "list-item" }, [
          _c(
            "a",
            {
              staticClass: "accordian-link collapsed",
              attrs: {
                href: "#",
                "data-toggle": "collapse",
                "data-target": "#accordian-4"
              }
            },
            [
              _c("div", { staticClass: "list-item-name" }, [
                _vm._v(
                  "\n            How does the Open Bazar protect its users? \n        "
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "list-item-icon" }, [
                _c("i", { staticClass: "fas fa-angle-down arrow-down-right" })
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "collapse", attrs: { id: "accordian-4" } }, [
          _c("div", { staticClass: "expandable-area" }, [
            _vm._v(
              "\n            We have a public follow-up team that follows up ads and agrees to post them on the site after filtering them to ensure user-friendly content, needs, market requirements, advertiser credibility, and direct communication.\n        "
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "list-item" }, [
          _c(
            "a",
            {
              staticClass: "accordian-link collapsed",
              attrs: {
                href: "#",
                "data-toggle": "collapse",
                "data-target": "#accordian-5"
              }
            },
            [
              _c("div", { staticClass: "list-item-name" }, [
                _vm._v(
                  "\n            How does the Open Bazar gain money? \n        "
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "list-item-icon" }, [
                _c("i", { staticClass: "fas fa-angle-down arrow-down-right" })
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "collapse", attrs: { id: "accordian-5" } }, [
          _c("div", { staticClass: "expandable-area" }, [
            _vm._v(
              '\n            As we explained earlier, the Open Bazar mechanism depends on advertising, but it provides features and services to its users to enhance the opportunity to profit faster through advertising and "stores", which in turn facilitates the sales of advertised goods, giving companies a greater presence online, Features are driven.\n        '
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "list-item" }, [
          _c(
            "a",
            {
              staticClass: "accordian-link collapsed",
              attrs: {
                href: "#",
                "data-toggle": "collapse",
                "data-target": "#accordian-6"
              }
            },
            [
              _c("div", { staticClass: "list-item-name" }, [
                _vm._v(
                  "\n            Can you market to a private company by advertising it on the Open Bazar? \n        "
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "list-item-icon" }, [
                _c("i", { staticClass: "fas fa-angle-down arrow-down-right" })
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "collapse", attrs: { id: "accordian-6" } }, [
          _c("div", { staticClass: "expandable-area" }, [
            _vm._v(
              '\n            No, because this is contrary to terms of use, and marketing depends on showing the same product or service that the company delivers through advertising. Instead, the open market allows sellers to "shop".\n        '
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "list-item" }, [
          _c(
            "a",
            {
              staticClass: "accordian-link collapsed",
              attrs: {
                href: "#",
                "data-toggle": "collapse",
                "data-target": "#accordian-7"
              }
            },
            [
              _c("div", { staticClass: "list-item-name" }, [
                _vm._v(
                  "\n            What is the next step after completing the sale? \n        "
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "list-item-icon" }, [
                _c("i", { staticClass: "fas fa-angle-down arrow-down-right" })
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "collapse", attrs: { id: "accordian-7" } }, [
          _c("div", { staticClass: "expandable-area" }, [
            _vm._v(
              "\n            The seller must deactivate the advertisement after the sale of the goods to avoid receiving calls and notifications about them.\n        "
            )
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/public/help.vue":
/*!**************************************!*\
  !*** ./resources/js/public/help.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _help_vue_vue_type_template_id_348c7499___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./help.vue?vue&type=template&id=348c7499& */ "./resources/js/public/help.vue?vue&type=template&id=348c7499&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _help_vue_vue_type_template_id_348c7499___WEBPACK_IMPORTED_MODULE_0__["render"],
  _help_vue_vue_type_template_id_348c7499___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/public/help.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/public/help.vue?vue&type=template&id=348c7499&":
/*!*********************************************************************!*\
  !*** ./resources/js/public/help.vue?vue&type=template&id=348c7499& ***!
  \*********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_help_vue_vue_type_template_id_348c7499___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./help.vue?vue&type=template&id=348c7499& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/public/help.vue?vue&type=template&id=348c7499&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_help_vue_vue_type_template_id_348c7499___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_help_vue_vue_type_template_id_348c7499___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);