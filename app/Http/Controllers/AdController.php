<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AdController extends Controller
{
    public function showAdDetails($id)
    {

        $url = "https://openbazar.nl/app-admin/fetch-ad-details-for-preview/$id";
        $response = Http::get($url);

        if($response->successful() && $response->json() && $response->json()['ad']){
            $ad = $response->json()['ad'];
            return view('preview' , compact('ad'));
        }else{
            return redirect('/');
        }


    }
}
