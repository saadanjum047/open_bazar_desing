<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use Omnipay\Omnipay;

class PaymentController extends Controller
{


    public $gateway;
 
    public function __construct()
    {
        $this->gateway = Omnipay::create('PayPal_Rest');
        $this->gateway->setClientId(env('PAYPAL_SANDBOX_CLIENT_ID'));
        $this->gateway->setSecret(env('PAYPAL_SANDBOX_CLIENT_SECRET'));
        $this->gateway->setTestMode(true); //set it to 'false' when go live

        // $this->gateway = Omnipay::create('PayPal_Rest');
        // $this->gateway->setClientId('AVxP-jU0lumxBdqgWJGwc-GydC3vPpRx2X3SrYY_j_b7gsSRy0DCflw6a7w7YA-7YTgxJMpJqAx68H5R');
        // $this->gateway->setSecret('EP0YEH1RwnV1qIw8Vgn21K9vaSEIk7gh1Ybp54kayQwUygeJQoA4-SqWCGIIm3f4B9mrNHVr0ZH1Q28v');
        // $this->gateway->setTestMode(true); //set it to 'false' when go live

    }



    public function paypal(Request $request)
    {


        try {
            $response = $this->gateway->purchase(array(
                'amount' =>  10 ,
                'currency' => 'EUR' ,
                'returnUrl' => '/payment-method' ,
                // 'returnUrl' => '/paymentsuccess?user_id=111&order_id=999',
                'cancelUrl' => '/payment-method',
            ))->send();
      
            if ($response->isRedirect()) {

                dd($response);
                $response->redirect(); // this will automatically forward the customer
            } else {
                // not successful

                return $response->getMessage();
            }
        } catch(Exception $e) {
            
            dd($e->getMessage());
            return $e->getMessage();
        }

        dd( 'S' , $response);


        // $provider = new PayPalClient;
        // // Through facade. No need to import namespaces
        // // $provider = PayPal::setProvider();
        // // $provider->setApiCredentials($config);
        // $provider->getAccessToken();
        // $provider->setCurrency('EUR');

        // $provider->createOrder([
        //     "intent"=> "CAPTURE",
        //     "purchase_units"=> [
        //         "amount"=> [
        //           "currency_code"=> "USD",
        //         //   "value"=? "100.00"
        //           "value"=> "10.00"
        //         ]
        //     ]
        // ]);
        // $provider->capturePaymentOrder($order_id); //order id from the createOrder step 
        

        // dd($provider);




    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
