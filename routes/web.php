<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/facebook', function () {
    return view('facebook');
});


Auth::routes();

Route::get('login-google', 'Auth\LoginController@redirectToGoogle');
Route::get('login-google/callback', 'Auth\LoginController@handleGoogleCallback');

Route::get('login-facebook', 'Auth\LoginController@redirectToFacebook');
Route::get('login-facebook/callback', 'Auth\LoginController@handleFacebookCallback');

Route::get('/accept-invitation' , 'BazarController@acceptInvitation' );

Route::post('/paypal' , 'BazarController@paypal' )->name('paypal');
Route::get('/payment-success' , 'BazarController@paymentSuccess' );



Route::get('/view-ad-details/{id}' , 'AdController@showAdDetails' );



// Route::get('/app-admin' , function(){
//     // dd(public_path());
//     // dd(url('/'));
//     // dd(base_path());
//     return Redirect::to(url('/') . '/app-admin/index.php');
//     // return File::get(base_path() . '/app-admin/index.php');
// } );

Route::get('/{path?}' , 'BazarController@vue' )->where('path', '^(?!.*api).*$[\/\w\.-]*');


// Route::get('/side', function(){
//     return view('side');
// });
